## Kali Linux

**Logo**

[![Kali Linux Logo](Logomark_and_Wordmark/kali-logo-dragon-black-white.png)](Logomark_and_Wordmark/)

**Wordmark**

[![Kali Linux Wordmark](Wordmark/kali-logo-black-white.png)](Wordmark/)

**Logomark** _(Dragon)_

[![Kali Linux Logomark](Logomark/kali-dragon-black-white.png)](Logomark/)
